## tangorpro-user 13 TQ3A.230605.009.A1 10100517 release-keys
- Manufacturer: google
- Platform: gs201
- Codename: tangorpro
- Brand: google
- Flavor: tangorpro-user
- Release Version: 13
- Kernel Version: 5.10.157
- Id: TQ3A.230605.009.A1
- Incremental: 10100517
- Tags: release-keys
- CPU Abilist: arm64-v8a
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/tangorpro/tangorpro:13/TQ3A.230605.009.A1/10100517:user/release-keys
- OTA version: 
- Branch: tangorpro-user-13-TQ3A.230605.009.A1-10100517-release-keys
- Repo: google_tangorpro_dump_30080
